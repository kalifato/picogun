# Picogun 

This is a video buffer to connect arcade boards to CRT tv sets. It's based on a THS7374 with DC coupled output for RGB, color calibration and a 74HC14 sync buffer outputting 75Ω ~400mV for SCART or TTL CSYNC. It's based on [THS7374 documentation](http://www.ti.com/product/THS7374), [RGB's sync message](https://www.arcade-projects.com/forums/index.php?threads/home-arcade-system.332/post-58949), [viletim's sync message](https://nfggames.com/forum2/index.php?topic=2560.msg17590#msg17590), [AV-Driver](http://etim.net.au/av-driver/jumpers/) and my own experimentation.

For audio just connect speakers to the JAMMA edge to get the best experience. This board doesn't have mounting holes to keep a small footprint; you can attach it using mounting tape.

*This is an experimental version not yet tested*

## Features

- Small footprint, no vias for video signals
- AC Coupled inputs and DC coupled outputs
- Regulable Red, Green and Blue colors
- Buffered sync output TTL or 75Ω ~350mV
- Dedicated ground plane to minimize interferences
- LPF filter on/off selectable by a jumper
- Minimalist design so you can fix a bad supergun or consolizations

## Settings

- JP1 closed: THS7374 filter ON
- JP1 open: THS7374 filter OFF
- JP2 closed: 75ohm ~400mV buffered SYNC output
- JP2 open: TTL buffered SYNC output

## BOM

- R1: 100K 0805
- R3, R4, R5: 75R 0805
- R7: 86R6 0805
- R6: 180R 0805
- R2: 470R 0805
- RV1, RV2, RV3: Bourns 3362P-1-102LF
- C1, C2, C3, C4: Ceramic capacitor 0805 X7R 100nF
- C5: Ceramic capacitor 0805 X7R 10nF
- C6: Ceramic capacitor 0805 X7R 1uF
- C7: Rubycon ML 47uF 10V / Rubycon PX 47uF 16V / Panasonic NHG 47uF 16V
- U2: Nexperia 74HC14D (pin 1 is connected to Si)
- U1: THS7374 / THS7373
- JP1, JP2: 2.54mm 2 position male pin header with Jumper
- FB1: MPZ2012S331A
